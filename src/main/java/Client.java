import command.Command;
import command.SaveCommand;
import funcInterfaceStudy.SimpleCalc;
import invoker.Macro;
import receiver.Editor;
import receiver.MockEditor;

public class Client {
    public static void main(String[] args) {
        System.out.println("Average of three numbers: ");
        SimpleCalc average = (a, b, c) -> (a + b + c) / 3;
        int averValue = (int) average.calculation(34, 45, 56);
        System.out.println(averValue);
        System.out.println("Maximal of three numbers: ");
        SimpleCalc maxVal = (a, b, c) -> Math.max(a, Math.max(b, c));
        int max = maxVal.calculation(3, 111, 8);
        System.out.println(max);
        System.out.println("Pattern command: ");
        Command commandOne = () -> {
            System.out.println("Command as lambda function: ");
        };
        commandOne.perform();
        ;
        Macro macro = new Macro();
        Editor editor = new MockEditor();
        Command commandTwo = new SaveCommand(editor);
        macro.record(editor::save);
        commandTwo.perform();
        Command commandThree = new Command() {
            @Override
            public void perform() {
                System.out.println("Invoke from anonymous class");
            }
        };
        commandThree.perform();
        System.out.println("Command as Object class ");
        Command commandFour=new SaveCommand(editor);
        commandFour.perform();
    }
}
