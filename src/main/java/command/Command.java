package command;
@FunctionalInterface
public interface Command {
   void perform();

}
