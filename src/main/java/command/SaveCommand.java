package command;

import receiver.Editor;

public class SaveCommand implements Command {
    private Editor editor;

    public SaveCommand(Editor editor) {
        this.editor = editor;
    }

    @Override
    public void perform() {
        editor.save();
    }
}
