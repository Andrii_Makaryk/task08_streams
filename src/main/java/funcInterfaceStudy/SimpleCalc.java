package funcInterfaceStudy;

@FunctionalInterface
public interface SimpleCalc {
    int calculation(int a,int b,int c);
}
