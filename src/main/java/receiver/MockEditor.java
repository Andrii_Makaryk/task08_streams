package receiver;

import java.util.ArrayList;
import java.util.List;

public class MockEditor implements Editor {
    @Override
    public void save() {
        System.out.println("Invoke as method reference");
    }
}
